public class App {
    public static void main(String[] args) throws Exception {
        
        Profesor objProfesor = new Profesor("Juan", "Herrera", "123456", 10);
        objProfesor.asignar_salon();

        Alumno objAlumno = new Alumno("Andrés", "Quintana", "98765");
        System.out.println(objAlumno.getNombre());
        objAlumno.asignar_salon();
        objAlumno.setNombre("José");
        System.out.println(objAlumno.getNombre());

    }
}
