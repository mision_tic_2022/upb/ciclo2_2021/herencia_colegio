public class Alumno extends Persona {
    /*************
     * Atributos
     *************/
    private String curso;

     /*************
     * Constructor
     *************/
    public Alumno(String nombre, String apellido, String id){
        super(nombre, apellido, id);
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    @Override
    public void asignar_salon(){
        System.out.println("Salón asignado para el Alumno "+super.getNombre()+" "+super.getApellido());
    }

}
