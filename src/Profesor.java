//Extends -> Hereda una clase
public class Profesor extends Persona {
    /*************
     * Atributos
     *************/
    private int salario;

     /*************
     * Constructor
     *************/
    public Profesor(String nombre, String apellido, String id, int salario){
        //super -> Llama al constructor de la clase heredada
        super(nombre, apellido, id);
        this.salario = salario;
    }

    /*************
     * Getters 
     * and 
     * Setter
     *************/

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    public void asignar_salon(){
        System.out.println("Salón asignado para el profesor "+getNombre()+" "+getApellido());
    }

}
